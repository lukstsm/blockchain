package com.sample.blockchain.app


import com.sample.blockchain.home.HomeActivity
import com.sample.blockchain.home.HomeActivityModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = arrayOf(HomeActivityModule::class, TransactionsFragmentProvider::class))
    internal abstract fun bindHomeActivity(): HomeActivity

}
