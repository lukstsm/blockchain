package com.sample.blockchain.app

import com.sample.blockchain.transactions.TransactionsFragment
import com.sample.blockchain.transactions.TransactionsFragmentModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class TransactionsFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(TransactionsFragmentModule::class))
    internal abstract fun provideStatusFragmentFactory(): TransactionsFragment
}
