package com.sample.blockchain.transactions.data

import io.reactivex.Observable
import javax.inject.Inject


class TransactionsProvider @Inject constructor(private val multiAddressApi: MultiAddressApi) {

    fun getTransactions(xpub: String): Observable<MultiAddress> {
        return multiAddressApi.fetchMultiAddress(xpub)
    }
}