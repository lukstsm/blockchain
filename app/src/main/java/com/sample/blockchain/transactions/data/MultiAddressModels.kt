package com.sample.blockchain.transactions.data

data class MultiAddress(val txs: List<Transaction>)

data class Transaction(val hash: String, val result: Long, val time: Long)
