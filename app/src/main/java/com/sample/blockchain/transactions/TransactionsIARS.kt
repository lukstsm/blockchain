package com.sample.blockchain.transactions

import com.sample.blockchain.architecture.InitialIntention
import com.sample.blockchain.transactions.data.Transaction


sealed class TransactionIntention {
    object Initial : TransactionIntention(),
                     InitialIntention

    object GetLastState : TransactionIntention()
    data class LoadTransactions(val xpub: String) : TransactionIntention()
}

sealed class TransactionAction {
    object Initial : TransactionAction()
    object GetLastState : TransactionAction()
    data class FetchTransactions(val xpub: String) : TransactionAction()
}

sealed class TransactionResult {
    object LastState : TransactionResult()
    data class Transactions(val transactions: List<Transaction>) : TransactionResult()
}

data class TransactionState(
        val transactions: List<Transaction> = emptyList()
)