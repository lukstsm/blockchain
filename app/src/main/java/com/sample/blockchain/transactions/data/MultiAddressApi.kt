package com.sample.blockchain.transactions.data

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface MultiAddressApi {

    @GET("/multiaddr")
    fun fetchMultiAddress(@Query("active") xpub: String): Observable<MultiAddress>

}