package com.sample.blockchain.transactions

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sample.blockchain.R
import com.sample.blockchain.transactions.data.Transaction
import kotlinx.android.synthetic.main.transaction.view.hash
import kotlinx.android.synthetic.main.transaction.view.time
import kotlinx.android.synthetic.main.transaction.view.value
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class TransactionsAdapter : RecyclerView.Adapter<TransactionsAdapter.TransactionViewHolder>() {

    private var transactions: List<Transaction> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.transaction, parent, false)
        return TransactionViewHolder(view)
    }

    override fun getItemCount() = transactions.size

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        //Normally would have model layers: network -> domain -> UI and consume the UI model here

        val transaction = transactions[position]

        val resultInBTC = transaction.result / Math.pow(10.0, 8.0)
        val resultFormatted = String.format("%.8f", resultInBTC)

        val date = Date(transaction.time * 1000)
        val formattedDate = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.UK).format(date)

        holder.itemView.hash.text = "Hash: ${transaction.hash}"
        holder.itemView.time.text = "Date: $formattedDate"
        holder.itemView.value.text = "Amount: $resultFormatted"

        val background = if (transaction.result < 0) android.R.color.holo_red_dark else android.R.color.holo_green_dark
        holder.itemView.setBackgroundResource(background)
    }

    fun setTransactions(transactions: List<Transaction>) {
        this.transactions = transactions
    }

    class TransactionViewHolder(view: View) : RecyclerView.ViewHolder(view)

}
