package com.sample.blockchain.transactions

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sample.blockchain.R
import dagger.android.support.AndroidSupportInjection
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.sample_fragment.progress
import kotlinx.android.synthetic.main.sample_fragment.sample_recycler_view
import kotlinx.android.synthetic.main.sample_fragment.view.sample_recycler_view
import javax.inject.Inject


class TransactionsFragment : Fragment() {

    private val XPUB = "xpub6CfLQa8fLgtouvLxrb8EtvjbXfoC1yqzH6YbTJw4dP7srt523AhcMV8Uh4K3TWSHz9oDWmn9MuJogzdGU3ncxkBsAC9wFBLmFrWT9Ek81kQ"

    companion object {
        fun newInstance(): TransactionsFragment {
            return TransactionsFragment()
        }
    }

    @Inject
    lateinit var presenter: TransactionsPresenter

    private var intentionsSubject = PublishSubject.create<TransactionIntention>()

    private lateinit var disposables: CompositeDisposable

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layout = inflater.inflate(R.layout.sample_fragment, container, false)

        layout.sample_recycler_view.layoutManager = LinearLayoutManager(activity)
        layout.sample_recycler_view.adapter = TransactionsAdapter()

        val dividerItemDecoration = DividerItemDecoration(activity, LinearLayoutManager.VERTICAL)
        dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.divider, null))

        layout.sample_recycler_view.addItemDecoration(dividerItemDecoration)

        return layout
    }

    override fun onStart() {
        super.onStart()
        disposables = CompositeDisposable(
            presenter.states()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::render),
            presenter.processIntentions(intentions())
        )
    }

    override fun onStop() {
        super.onStop()
        disposables.dispose()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroy()
    }


    private fun intentions() = Observable.merge(
        Observable.just(TransactionIntention.Initial),
        Observable.just(TransactionIntention.LoadTransactions(XPUB)),
        intentionsSubject
    )

    private fun render(state: TransactionState) {
        progress.visibility = if (state.transactions.isEmpty()) View.VISIBLE else View.GONE
        (sample_recycler_view.adapter as TransactionsAdapter).setTransactions(state.transactions)
        sample_recycler_view.adapter.notifyDataSetChanged()
    }

}