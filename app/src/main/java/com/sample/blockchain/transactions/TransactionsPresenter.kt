package com.sample.blockchain.transactions

import com.sample.blockchain.architecture.MVIBasePresenter
import javax.inject.Inject


class TransactionsPresenter @Inject constructor(
        interactor: TransactionsInteractor
) : MVIBasePresenter<TransactionIntention, TransactionAction, TransactionResult, TransactionState>(interactor) {

    override val defaultState: TransactionState
        get() = TransactionState()

    override val lastStateIntention: TransactionIntention
        get() = TransactionIntention.GetLastState

    override fun intentionToActionMapper(): (TransactionIntention) -> TransactionAction = {
        when (it) {
            is TransactionIntention.Initial -> TransactionAction.Initial
            is TransactionIntention.GetLastState -> TransactionAction.GetLastState
            is TransactionIntention.LoadTransactions -> TransactionAction.FetchTransactions(it.xpub)
        }
    }

    override fun stateReducer(): (TransactionState, TransactionResult) -> TransactionState = { prevState, result ->
        when (result) {
            is TransactionResult.LastState -> prevState.copy()
            is TransactionResult.Transactions -> prevState.copy(transactions = result.transactions)
        }
    }
}