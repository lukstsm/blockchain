package com.sample.blockchain.transactions

import com.sample.blockchain.architecture.MVIInteractor
import com.sample.blockchain.transactions.data.TransactionsProvider
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class TransactionsInteractor @Inject constructor(
        private val transactionsProvider: TransactionsProvider
) : MVIInteractor<TransactionAction, TransactionResult> {

    override fun actionProcessor(): ObservableTransformer<in TransactionAction, out TransactionResult> {
        return ObservableTransformer {
            it.observeOn(Schedulers.io())
                .flatMap { action ->
                    when (action) {
                        is TransactionAction.Initial -> getLastState()
                        is TransactionAction.GetLastState -> getLastState()
                        is TransactionAction.FetchTransactions -> fetchTransactions(action.xpub)
                    }
                }
        }
    }

    private fun getLastState(): Observable<TransactionResult> = Observable.just(TransactionResult.LastState)

    internal fun fetchTransactions(xpub: String) = transactionsProvider.getTransactions(xpub)
        .map { TransactionResult.Transactions(it.txs) }

}