package com.sample.blockchain.transactions

import com.sample.blockchain.transactions.data.MultiAddress
import com.sample.blockchain.transactions.data.Transaction
import com.sample.blockchain.transactions.data.TransactionsProvider
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class TransactionsInteractorTest {

    private val XPUB = "fgsery5ygdfg4534"

    private val transactions = listOf(
        Transaction("123abc", -3453, 54634563456),
        Transaction("456abc", 346456, 54634563999),
        Transaction("789abc", -54756, 54634599999)
    )

    private val multiAddress = MultiAddress(transactions)

    @Mock
    private lateinit var transactionsProvider: TransactionsProvider

    private lateinit var interactor: TransactionsInteractor

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        `when`(transactionsProvider.getTransactions(anyString()))
            .thenReturn(Observable.just(multiAddress))

        interactor = TransactionsInteractor(transactionsProvider)
    }

    @Test
    fun interactorReturnsTransactionsResult() {
        interactor.fetchTransactions(XPUB)
            .test()
            .assertValue { result -> result.transactions == transactions }
    }


}